// 1. Змінну можна оголосити за допомогою var, let, const.
// 2. string використовується для представлення послідовності символів і керування нею.
// const string-1 = "рядок з подвійними лаками"
// const string-2 = 'рядок з однинарними лапками'
// const string-3 = `рядок з зворотніми лапками`
// 3. Для перевірки типу данних змінної використовуть опертаорк typeof
//console.log(typeof 43);
// 4.  '1' + 1 = 11. Js в даному прикладі використовує конкатенацію замість операції додавання чисел

"use strict"

let number = 15;
console.log(typeof number);

let name = "Olexiy";
let lastName = "Vatsenko";
console.log(`Мене звати ${name},${lastName}`);

let Number = 20;
console.log(`Це мій ${Number} рядок`);